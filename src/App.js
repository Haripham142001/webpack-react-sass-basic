import React from "react";
import { constant } from "services";
import Hello from "features/test/Hello";

const App = () => {
  return (
    <div>
      <Hello />
    </div>
  );
};

export default App;
